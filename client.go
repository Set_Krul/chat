package main

import (
	"flag"
	"net"
	"./configuration"
	"bufio"
	"os"
	"fmt"
)

func main() {
	flag.Parse()

	conn, _ := net.Dial(configuration.PROTOCOL_TYPE, *targetAddr)
	scanner := bufio.NewScanner(os.Stdin)
	reader := bufio.NewReader(conn)
	writer := bufio.NewWriter(conn)
	go func() {
		for {
			line, _ := reader.ReadString('\n')
			fmt.Println(line)
		}
	}()
	for scanner.Scan() {
		writer.WriteString(scanner.Text())
		fmt.Println(scanner.Text())
	}
}
