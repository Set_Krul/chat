package resources

//tests
const (
	Target_chat_server_address string = "target chat server address"
	Test_message_length        string = "test message length"
	Test_connection_number     string = "test connection number"
	Test_duration_in_seconds   string = "test duration in seconds"

	Benchmarking    string = "Benchmarking:"
	Clients_running string = "clients, running"
	Bytes           string = "bytes,"
	Sec             string = "sec."
	Speed           string = "Speed:"
	Request_sec     string = "request/sec,"
	Response_sec    string = "response/sec"
	Requests        string = "Requests:"
	Responses       string = "Responses:"
)

//main
const (
	Server_fail          string = "Server listening failed. Exit."
	Connection_fail      string = "Connection accepting failed."
	Connection_accepting string = "Connection accepting"
	Server_already_run   string = "Server already run"
)

//client
const (
	Connection_disconect string = "Connection disconect"
	Upload_start         string = "Upload start\n"
	Upload_finish        string = "Upload finish\n"
	Upload_error         string = "Upload error\n"
	File_so_big          string = "File so big max size 250 mb \n"
)
