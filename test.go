package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"sync"
	"sync/atomic"
	"time"
	"./configuration"
	"./resources"
)

var (
	targetAddr  = flag.String("a", "127.0.0.1"+configuration.CONNECTION_PORT, resources.Target_chat_server_address)
	testMsgLen  = flag.Int("l", 8, resources.Test_message_length)
	testConnNum = flag.Int("c", 3, resources.Test_connection_number)
	testSeconds = flag.Int("t", 5, resources.Test_duration_in_seconds)
)

func main() {
	flag.Parse()
	var (
		outNum uint64
		inNum  uint64
		stop   uint64
	)

	testMsg := make([]byte, *testMsgLen)
	for i := 0; i < (*testMsgLen - 1); i++ {
		testMsg[i] = 'a'
	}
	testMsg[*testMsgLen-1] = '\n'

	go func() {
		time.Sleep(time.Second * time.Duration(*testSeconds))
		atomic.StoreUint64(&stop, 1)
	}()
	waitGroup := new(sync.WaitGroup)

	for i := 0; i < *testConnNum; i++ {
		waitGroup.Add(1)
		messageLen := len(testMsg)
		read := make([]byte, messageLen)
		if conn, err := net.DialTimeout(configuration.PROTOCOL_TYPE, *targetAddr, time.Minute*99999); err == nil {
			go func() { // goroutine: reader
				for {
					for rest := messageLen; rest > 0; {
						i, err := conn.Write(testMsg)
						rest -= i
						if err != nil {
							log.Println(err)
							break
						}
					}
					atomic.AddUint64(&outNum, 1)
					if atomic.LoadUint64(&stop) == 1 {
						break
					}
				}
				waitGroup.Done()
			}()
			go func() {
				for {
					for rest := messageLen; rest > 0; {
						i, err := conn.Read(read)
						rest -= i
						if err != nil {
							log.Println(err)
							break
						}
					}
					atomic.AddUint64(&inNum, 1)
					if atomic.LoadUint64(&stop) == 1 {
						break
					}
				}
			}()
		} else {
			log.Println(err)
		}
	}
	waitGroup.Wait()

	fmt.Println(resources.Benchmarking, *targetAddr)
	fmt.Println(*testConnNum, resources.Clients_running, *testMsgLen, resources.Bytes, *testSeconds, resources.Sec)
	fmt.Println()
	fmt.Println(resources.Speed, outNum/uint64(*testSeconds), resources.Request_sec, inNum/uint64(*testSeconds), resources.Response_sec)
	fmt.Println(resources.Requests, outNum)
	fmt.Println(resources.Responses, inNum)
}
