package utils

import (
	"runtime"
	"os"
	"time"
	"../configuration"
)

func MemMotitor() {
	go func() {
		for {
			var mem runtime.MemStats
			runtime.ReadMemStats(&mem)
			if mem.TotalAlloc > configuration.MEM_LIMIT {
				os.Exit(1)
			}
			time.Sleep(15 * time.Second)
		}
	}()
}
