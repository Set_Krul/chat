package utils

import (
	"os"
	"strings"
	"../configuration"
)

func DirExists(path string) {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		os.Mkdir(path, os.ModePerm)
	}
}

func FileExists(path string) bool {
	_, err := os.Stat(path)
	return err == nil
}

func CorrectSize(path string) bool {
	file, e := os.Stat(path)
	if e != nil || file.Size() > configuration.MAX_FILE_UPLOAD_SIZE {
		return false
	}
	return true
}

func GetFileNameFromPath(path string) string {
	return path[strings.LastIndex(path, "/")+1:]
}

func FixTerminalPathToFile(terminalPath string) string {
	s := terminalPath[:len(terminalPath)-1]
	return strings.Replace(s, string('\\'), "/", len(s))
}
