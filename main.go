package main

import (
	"log"
	"net"
	"os"
	"./models"
	"./configuration"
	"./utils"
	"./resources"
	"fmt"
)

func main() {
	utils.MemMotitor()
	chatRoom := NewChatRoom()
	listener, err := net.Listen(configuration.PROTOCOL_TYPE, configuration.CONNECTION_PORT)
	if err != nil {
		fmt.Println(resources.Server_fail)
		os.Exit(1)
	}
	for {
		if err == nil {
			conn, err := listener.Accept()
			if err != nil {
				fmt.Println(resources.Connection_fail)
				conn.Close()
				continue
			}
			fmt.Println(resources.Connection_accepting)
			chatRoom.Joins <- conn
		} else {
			log.Fatalf(resources.Server_already_run)
			os.Exit(1)
		} 
	}
}

func NewChatRoom() *models.ChatRoom {
	chatRoom := &models.ChatRoom{
		Clients: make(map[int]*models.Client),
		LastSid: -1,
		Joins:   make(chan net.Conn),
		In:      make(chan string),
		Out:     make(chan string),
	}
	chatRoom.Listen()
	return chatRoom
}
