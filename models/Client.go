package models

import (
	"bufio"
	"path/filepath"
	"../utils"
	"io/ioutil"
	"../configuration"
	"../resources"
	"net"
	"sync"
	"fmt"
)

var ClientNow *Client

type Client struct {
	sid              int
	incoming         chan string
	outgoing         chan string
	isConnectionDead chan bool
	isReaderDead     chan bool
	isWriterDead     chan bool
	reader           *bufio.Reader
	writer           *bufio.Writer
	chatRoom         *ChatRoom
	connection       net.Conn
	clientMutex      sync.Mutex
}

func (client *Client) Write() {
	for {
		select {
		case <-client.isWriterDead:
			return
		case data := <-client.outgoing:
			client.writer.WriteString(data)
			client.writer.Flush()
		}
	}
}

func (client *Client) Listen() {
	//go client.Read()
	go client.Write()
}

func (client *Client) Read() {
	for {
		select {
		case <-client.isReaderDead:
			return
		default:
			line, err := client.reader.ReadString('\n')
			if err != nil {
				fmt.Println(resources.Connection_disconect)
				client.LeaveAndDelete()
				continue
			}
			if line != "" {
				path := utils.FixTerminalPathToFile(line)
				absPath, _ := filepath.Abs(path)
				if utils.FileExists(absPath) {
					uploadFile(utils.GetFileNameFromPath(path), absPath, configuration.LOCAL_PATH_FoR_FiLE, client)
				} else {
					ClientNow = client
					client.incoming <- line
					client.connection.Write([]byte(line))
				}
			}
		}
	}
}

func uploadFile(name, absPath, newDirectory string, client *Client) {
	if utils.CorrectSize(absPath) {
		client.incoming <- resources.Upload_start
		contents, _ := ioutil.ReadFile(absPath)
		utils.DirExists(newDirectory)
		if e := ioutil.WriteFile(newDirectory+"/"+name, contents, 0644); e == nil {
			client.incoming <- resources.Upload_finish
		} else {
			client.incoming <- resources.Upload_error
		}
	} else {
		client.incoming <- resources.File_so_big
	}
}

func (client *Client) LeaveAndDelete() {
	chatRoom := *client.chatRoom
	sid := client.sid

	chatRoom.chatMutex.Lock()

	delete(chatRoom.Clients, sid)

	client.clientMutex.Lock()

	client.isReaderDead <- true
	client.isWriterDead <- true

	client.reader = nil
	client.writer = nil

	client.connection.Close()
	client.connection = nil
	client.isConnectionDead <- true
	defer client.clientMutex.Unlock()
	defer chatRoom.chatMutex.Unlock()
}
