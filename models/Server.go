package models

import (
	"net"
	"bufio"
	"sync"
)

type ChatRoom struct {
	LastSid   int
	Clients   map[int]*Client
	Joins     chan net.Conn
	In        chan string
	Out       chan string
	chatMutex sync.Mutex
}

func (chatRoom *ChatRoom) Broadcast(data string) {
	for _, client := range chatRoom.Clients {
		if ClientNow != nil && client == ClientNow {
			ClientNow = nil
			continue
		}
		client.outgoing <- data
		client.connection.Write([]byte(data))
	}
}

func (chatRoom *ChatRoom) Join(connection net.Conn) {
	chatRoom.chatMutex.Lock()
	newClientId := chatRoom.LastSid + 1
	chatRoom.LastSid = newClientId
	client := NewClient(newClientId, chatRoom, connection)

	_, keyExist := chatRoom.Clients[newClientId]
	if ! keyExist {
		chatRoom.Clients[newClientId] = &client
	}
	go func() {
		for {
			select {
			case <-client.isConnectionDead:
				return
			case data := <-client.incoming:
				chatRoom.In <- data
			}
		}
	}()
	defer chatRoom.chatMutex.Unlock()
}
func NewClient(sid int, chatRoom *ChatRoom, connection net.Conn) Client {
	writer := bufio.NewWriter(connection)
	reader := bufio.NewReader(connection)

	client := Client{
		sid:              sid,
		chatRoom:         chatRoom,
		connection:       connection,
		incoming:         make(chan string),
		outgoing:         make(chan string),
		reader:           reader,
		writer:           writer,
		isConnectionDead: make(chan bool),
		isReaderDead:     make(chan bool),
		isWriterDead:     make(chan bool),
	}
	client.Listen()
	return client
}

func (chatRoom *ChatRoom) Listen() {
	go func() {
		for {
			select {
			case data := <-chatRoom.In:
				chatRoom.Broadcast(data)
			case conn := <-chatRoom.Joins:
				chatRoom.Join(conn)
			}
		}
	}()
}
