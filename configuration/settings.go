package configuration

const (
	LOCAL_PATH_FoR_FiLE string = "localData"
	PROTOCOL_TYPE       string = "tcp"
	CONNECTION_PORT     string = ":8082"

	MAX_FILE_UPLOAD_SIZE int64 = 1024*250*1024
	MEM_LIMIT uint64 = 1024*1024*8*50
)
